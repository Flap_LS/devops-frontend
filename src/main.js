import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/store'
import vuetify from './plugins/vuetify'
//import axios from "axios";

Vue.mixin({
    methods: {
        getHeader(serviceNumber) {
            if (serviceNumber === 1) {
                return {
                    baseURL: this.firstHostname,
                    headers: {
                        'Access-Control-Allow-Origin': '*',
                        'Access-Control-Allow-Headers': '*',
                        'Access-Control-Allow-Credentials': 'true'
                    }
                }
            }
        }
    },
    data: function () {
        return {
            get firstHostname() {
                return "http://localhost:8000";
            },
            get awaitTimer() {
                return 1000;
            },
            get errorTimer() {
                return 5000;
            }
        }
    }
})

new Vue({
    store,
    router,
    vuetify,
    render: h => h(App),
}).$mount('#app')
